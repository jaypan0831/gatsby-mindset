import React from 'react';

import Head from '../components/head'
import Layout from '../components/layout'

export default function AboutPage() {
  return (
    <Layout>
      <Head title='About' />
      <h1>About</h1>
    </Layout>
  );
}
